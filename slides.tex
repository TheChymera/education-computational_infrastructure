\input{slides/header.tex}
\input{common_header.tex}
\title[Computational Infrastructure for Data Science]{Computational Infrastructure for Data Science}
\subtitle{\href{https://bitbucket.org/TheChymera/education-computational-infrastructure}{[ bitbucket.org/TheChymera/education-computational-infrastructure ]}}
\author[Horea Christian]{Horea Christian\\\href{https://twitter.com/TheChymera}{\small\texttt{[ @TheChymera ]}}}
\institute{Department of Biological Engineering, MIT}
\begin{document}
	\begin{frame}
		\titlepage
	\end{frame}
	\section{Data Analysis}
		\subsection{More than an afterthought.}
			\begin{frame}{Figures and Statistics are a \textit{direct output} of your Data Analysis.}
				The Bad News:
				\begin{itemize}
					\item Data analysis is a more critical failure point than your wet work.
					\item Your results are never analysis-invariant.
					\item Generally, your results do not even exist in the absence of analysis.
				\end{itemize}
				\vspace{1em}
				The Good news:
				\begin{itemize}
					\item Data analysis can be fully reproducible.
					\item Data analysis can be fully transparent and documented.
					\item \textit{With the right tools}, a stable parameter set can feasibly be determined.
				\end{itemize}
			\end{frame}
			\begin{frame}{The Spectrum on Which You Can Mess Up}
				\noindent\makebox[\textwidth][c]{%
					\begin{minipage}{.49\textwidth}
						\begin{figure}
							\centering
							\includegraphics[width=0.65\textwidth]{img/mcv.png}
							\caption{Brain map correlations from multi-center testing of the same second-level hypothesis in a stimulus-evoked design. \cite{mcv}}
						\end{figure}
					\end{minipage}
					\begin{minipage}{.49\textwidth}
						\begin{figure}
							\centering
							\vspace{.2em}
							\includegraphics[width=0.91\textwidth]{img/bv.png}
							\caption{Differences in estimated cortical thickness for analysis using different builds of the selfsame software. \cite{bv}}
						\end{figure}
					\end{minipage}
				}\vspace{3em}
			\end{frame}
	\section{Software Environments}
		\subsection{Don't build your project on a house of cards.}
			\begin{frame}{No software stands on its own.}
                                \vspace{-2em}
                                \begin{figure}
                                        \centering
                                        \includedot[width=0.75\textwidth]{data/dependencies}
                                \end{figure}
			\end{frame}
			\begin{frame}{And the complexity is unmanageable...}
				\noindent\makebox[\textwidth][c]{%
					\begin{minipage}{.49\textwidth}
						\begin{figure}
							\centering
							\includegraphics[width=0.75\textwidth]{img/ngs_small.png}
							\vspace{-.6em}
							\caption{Neuroimaging dependency graph with minimal features (\SI{\sim 550}{packages}) \cite{ng}}
						\end{figure}
					\end{minipage}
					\begin{minipage}{.49\textwidth}
						\begin{figure}
							\centering
							\includegraphics[width=0.75\textwidth]{img/ngl_small.png}
							\vspace{-.6em}
							\caption{Neuroimaging dependency graph with full features (\SI{\sim 3500}{packages}) \cite{ng}}
						\end{figure}
					\end{minipage}
				}\vspace{3em}
			\end{frame}
			\begin{frame}{... unless you have an infrastructure with appropriate features.}
				\noindent\makebox[\textwidth][c]{%
					\vspace{.5em}
					\begin{minipage}{.23\textwidth}
						\begin{figure}
							\centering
							\includegraphics[width=0.75\textwidth]{img/gentoo.png}
							\vspace{-.6em}
						\end{figure}
					\end{minipage}
					\begin{minipage}{.52\textwidth}
						\begin{figure}
							\centering
							\includegraphics[width=1\textwidth]{img/samri}
							\vspace{-.6em}
						\end{figure}
					\end{minipage}
					\begin{minipage}{.23\textwidth}
						\begin{figure}
							\centering
							\includegraphics[width=0.75\textwidth]{img/pythontex}
							\vspace{-.6em}
						\end{figure}
					\end{minipage}
				}\vspace{3em}
			\end{frame}
	\section{Options}
		\subsection{Where can we best deploy these features.}
			\begin{frame}{Physical machine [\SIrange{0}{\sim 10000}{USD}]}
				Pros:
				\begin{itemize}
					\item Cheapest option (after a few years)
				\end{itemize}
				Cons:
				\begin{itemize}
					\item Non-flexible capacity
					\item Manual hardware maintenance
					\item VPN lock-in
					\item Encourages bad practices (i.e. usage like a personal computer)
				\end{itemize}
			\end{frame}
			\begin{frame}{MIT MGHPCC Compute Cluster [\SIrange{5000}{10000}{USD/yr}]}
				Pros:
				\begin{itemize}
					\item You can tell people you are using “Openmind”, which sounds cool.
				\end{itemize}
				Cons:
				\begin{itemize}
					\item Most expensive option
					\item Lack of administrator access
					\item Custom job submission interface
					\item VPN lock-in
					\item Capacity isn't even that large (\SI{80}{vCPU}, \SI{600}{GB RAM}),
					\item but you still have to share it with dozens of others.
				\end{itemize}
			\end{frame}
			\begin{frame}{AWS [\SIrange{\sim 5500}{\sim 9000}{USD/yr}]}
				Pros:
				\begin{itemize}
					\item Experience with AWS possibly looks good on your CV.
					\item Additional discounts may be available upon Amazon Grant application.
				\end{itemize}
				Cons:
				\begin{itemize}
					\item Machines need to be started on-demand to avoid scandalous costs.
					\item Machine administration via a slow and convoluted web platform
					\item Limited storage space (\SI{265}{GB}, extra costs beyond that)
				\end{itemize}
			\end{frame}
			\begin{frame}{OVH Dedicated Server [\SIrange{\sim 2000}{\sim 3500}{USD/yr}]}
				Pros:
				\begin{itemize}
					\item Flexible capacity
					\item Cheapest option (for the first 2-3 years)
					\item No VPN lock-in
					\item Full administrator access
					\item Already pilot-tested
				\end{itemize}
				Cons:
				\begin{itemize}
					\item No
				\end{itemize}
			\end{frame}
			\begin{frame}{Recommended OVH Options}
					\begin{minipage}{.49\textwidth}
						\centering \textbf{INFRA-2}
						\begin{itemize}
							\item Intel 8c/16t @ \SIrange{3.7}{5.0}{\giga\hertz}
							\item \SI{128}{GB} DDR4 ECC RAM @ \SI{2666}{\mega\hertz}
							\item 3 $\times$ \SI{4}{TB} hard drives
							\item \SI{1}{Gbps}, unmetered
							\item Christian recommends: up to 3 main users
							\item \SI{1998.00}{USD/yr}
						\end{itemize}
					\end{minipage}
					\begin{minipage}{.49\textwidth}
						\centering \textbf{INFRA-3}
						\begin{itemize}
							\item AMD 16c/32t @ \SIrange{3.1}{3.8}{\giga\hertz}
							\item \SI{256}{GB} DDR4 ECC RAM @ \SI{2400}{\mega\hertz}
							\item 3 $\times$ \SI{4}{TB} hard drives
							\item \SI{1}{Gbps}, unmetered
							\item Christian recommends: up to 6 main users
							\item \SI{3369.60}{USD/yr}
						\end{itemize}
					\end{minipage}
			\end{frame}
	\section{References}
		\bibliographystyle{abbrv}
		\bibliography{./bib}
\end{document}
